from datetime import datetime
from zipfile import ZipFile
from pathlib import Path
import codecs
import messages
import logging
import shutil
import os
import re


def init_conversion(data):
    # Iterate through all the found .txt files and do the conversion
    if data['input_folder_path'] != '':
        found_files = list(Path(data['input_folder_path']).rglob("*.[tT][xX][tT]"))
        if len(found_files) > 0:
            for i, row in enumerate(found_files):
                result = conversion(row, data)

                if result != 'OK':
                    messages.show_error_msg(
                        'Hiba!',
                        'Hiba történt a fájl konvertálása közben. Nézd meg a log fájlt!'
                    )
                    break

            # Show success message if there were no errors
            messages.show_info_msg(
                'Siker!',
                'Kész csoda, de úgy tűnik, sikeres volt a konvertálás!'
            )
        else:
            messages.show_warning_msg(
                'Figyeljé\' oda!',
                'A megadott útvonalon nem található .txt fájl!',
            )
    else:
        messages.show_warning_msg(
            'Figyeljé\' oda!',
            'Nem választottál bemeneti mappát!',
        )


def conversion(txt_file_path, data):
    # Get current datetime
    now = datetime.now()

    # Set the correct paths and filenames
    relative_txt_file_path = str(Path(txt_file_path)).replace(str(Path(data['input_folder_path'])), '')
    txt_file_basename = os.path.basename(str(Path(txt_file_path)))
    txt_file_path_no_ext = relative_txt_file_path.replace('.txt', '')
    new_pot_file_path = str(Path(data['output_folder_path'])) + txt_file_path_no_ext
    zip_file_path = new_pot_file_path + '.zip'

    try:
        # Get the .txt file
        # Read the sum of lines
        # Iterate through and save it to the new .pot file
        with codecs.open(str(Path(txt_file_path)), 'r', encoding='utf-8') as textFile:
            line_count = 0
            text_file_lines = textFile.readlines()

            os.makedirs(os.path.dirname(Path(str(new_pot_file_path) + '.pot')), exist_ok=True)
            with codecs.open(str(Path(new_pot_file_path)) + '.pot', 'a', encoding='utf-8') as potFile:
                # Header
                potFile.write('# ' + str(data['translation_title']) + ' - ' + str(txt_file_basename))
                potFile.write('\n')
                potFile.write('# ' + str(data['translator_full_name']) + ' <' + str(
                    data['translator_email']) + '>, ' + now.strftime("%Y"))
                potFile.write('\n')
                potFile.write('#')
                potFile.write('\n')
                potFile.write('msgid ""')
                potFile.write('\n')
                potFile.write('msgstr ""')
                potFile.write('\n')
                potFile.write('"Project-Id-Version: ' + data['package_version'] + '\\n"')
                potFile.write('\n')
                potFile.write('"Report-Msgid-Bugs-To: ' + str(data['translator_email']) + '\\n"')
                potFile.write('\n')
                potFile.write('"POT-Creation-Date: ' + now.strftime("%Y-%m-%d %H:%M:%S") + '\\n"')
                potFile.write('\n')
                potFile.write('"PO-Revision-Date: ' + now.strftime("%Y-%m-%d %H:%M:%S") + '\\n"')
                potFile.write('\n')
                potFile.write('"Last-Translator: ' + str(data['translator_full_name']) + ' <' + str(
                    data['translator_email']) + '>\\n"')
                potFile.write('\n')
                potFile.write('"Language-Team: ' + str(data['translation_target_lang']) + '\\n"')
                potFile.write('\n')
                potFile.write('"Language: ' + str(data['translation_target_lang_code']) + '\\n"')
                potFile.write('\n')
                potFile.write('"MIME-Version: 1.0\\n"')
                potFile.write('\n')
                potFile.write('"Content-Type: text/plain; charset=UTF-8\\n"')
                potFile.write('\n')
                potFile.write('"Content-Transfer-Encoding: 8bit\\n"')
                potFile.write('\n')
                potFile.write('"Plural-Forms: nplurals=2; plural=(n != 1);\\n"')

                # Translation strings
                for line in text_file_lines:
                    line_count += 1

                    if line.find('. '):
                        string_array = re.split('(?<=[.!?]) +', line)

                        if len(string_array) > 1:
                            for sentence in string_array:
                                if sentence != '. ':
                                    potFile.write('\n')
                                    potFile.write('\n')
                                    potFile.write('#: ' + txt_file_basename + ':' + str(line_count))
                                    potFile.write('\n')
                                    potFile.write('msgctxt "' + txt_file_basename + '_' + str(line_count) + '"')
                                    potFile.write('\n')
                                    potFile.write('msgid "' + sentence.rstrip() + '"')
                                    potFile.write('\n')
                                    potFile.write('msgstr ""')
                        else:
                            potFile.write('\n')
                            potFile.write('\n')
                            potFile.write('#: ' + txt_file_basename + ':' + str(line_count))
                            potFile.write('\n')
                            potFile.write('msgctxt "' + txt_file_basename + '_' + str(line_count) + '"')
                            potFile.write('\n')
                            potFile.write('msgid "' + line.strip() + '"')
                            potFile.write('\n')
                            potFile.write('msgstr ""')

                potFile.close()

        # Create .po file from .pot
        pot_file = Path(str(new_pot_file_path) + '.pot')
        po_file = Path(str(new_pot_file_path) + '_' + str(data['translation_target_lang_code']) + '.po')
        shutil.copy(
            pot_file,
            po_file
        )

        # Create a single .zip file from the above two files
        zip_obj = ZipFile(str(zip_file_path), 'w')
        zip_obj.write(pot_file, os.path.basename(pot_file))
        zip_obj.write(
            po_file,
            os.path.basename(po_file))
        zip_obj.close()

        return 'OK'
    except Exception as err:
        logging.basicConfig(filename='error_log.txt',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            encoding='utf-8',
                            level=logging.ERROR)
        logging.exception(".txt to .pot konvertálási hiba.")
        return 'ERROR'
