# Weblate kisegítő applikáció (1.0.1)

---

## Információ

Kisegítő applikáció a Weblate számára, amely az egyszerű .txt fájlokból Gettext .pot (és .po) fájlokat készít, 
valamint a Weblate-ről letöltött .csv fájlokból ismét .txt fájlt. Az applikáció kezeli a mondatok szegmentálását is.
Mindemellett az applikáció figyelembe veszi a mappa szerkezetet is, tehát olyan struktúrában menti el a fájlokat,
ahogyan beolvasta őket.

- Python 3.10-ben készült
- Tkinter GUI-t használ
- MacOS és Windows alatt is működik

### Oké, de mire jó ez az app valójában?

A fentiekből persze nem egyértelmű, hogy miért is kellett létrehozni egy ilyen applikációt.
Az egészre azért volt szükség, mert a Weblate, bár támogatja a sima .txt-ben, soronként elmentett szövegek beolvasását,
sajnos nem mindig importálja őket helyesen, valamint a mondatok szegmentálását is figyelmen kívül hagyja.
Ezt a problémát próbálja orvosolni ez a **Weblate kisegítő**, hogy a sima .txt-ből .pot és .po fájlokat hoz létre, amiket
.zip/be csomagol, és amit aztán már fel tudunk tölteni a Weblate-re is, méghozzá szegmentáltan.

A **.csv to .txt** konvertálás pedig azért kell, mert sajnos a Weblate által generált .po fájlokat nem lehet könnyen
ismét .txt-té alakítani. Szerencsére a Weblate megengedi, hogy más formátumban töltsd le a lefordított szövegeket, mint
amilyenben feltöltötted. Ezért .csv-ben letöltve, az app segítségével már át tudod alakítani az eredeti .txt-té.

## Használat

A bal felső oldalon található a Gettext információs rész, ahol a POT fájlokhoz szükséges adatokat kell megadnod. Ez nem
kötelező, de a Weblate ezekből az adatokból is olvas, szóval inkább csak ajánlott.

![img.png](assets/images/gettext_part.png)

A bal alsó részen található a **konvertálási mód** kiválasztása, amit érdemes rögtön elsőként beállítani:

1. .txt to .pot
2. .csv to .txt

Itt található az **Indítás** gomb is, amellyel elindítod a konvertálást.

![img.png](assets/images/conversion_mode.png)

A jobb oldalon található az applikációnak az a része, ahol a bemeneti (input) és kimeneti (output) mappákat kell
kiválasztanod. Mielőtt ezt megtennéd

![img.png](assets/images/folder_selection_part.png)

## Közreműködés

Mivel az applikációt nem csak magamnak készítettem, ráadásul rengeteg a különböző fájl, ezért megeshet,
hogy a kód javításra szorul majd. Illetve, az sem gond, ha csak egy új funkcióval szeretnéd bővíteni. Hogyan tudsz
mindehhez hozzájárulni:

- Forkold a projektet
- Hozz létre egy saját _feature branchet_ (```git checkout -b my-new-feature```)
- Véglegesítsd a változtatásaidat (```git commit -am 'Hozzáadtam egy fasza funkciót'```)
- Told fel a branchedet a repóba (```git push origin my-new-feature```)
- Hozz létre egy új _merge requestet_

