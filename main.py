import tkinter.scrolledtext as st
import tkinter.filedialog as fd
import tkinter.ttk as ttk
from pathlib import Path
import tkinter as tk
import messages
import txtToPot
import csvToTxt


class SelectionFrame(ttk.Frame):
    def __init__(self, container):
        super().__init__(container)

        self.testingInherit = 'valami'

        # Handle input folder selection
        def open_input_folder():
            self.selected_input_folder = fd.askdirectory()
            self.entry_input_folder.configure(state='normal')
            self.string_input_folder.set(str(Path(self.selected_input_folder)))
            self.entry_input_folder.configure(state='readonly')

            # Filter found files according to conversion mode
            if self.selected_input_folder != '':
                foundFiles = []
                if container.selected_conversion_mode == '.txt -> .pot':
                    foundFiles = list(Path(self.selected_input_folder).rglob("*.[tT][xX][tT]"))
                elif container.selected_conversion_mode == '.csv -> .txt':
                    foundFiles = list(Path(self.selected_input_folder).rglob("*.[cC][sS][vV]"))

                if len(foundFiles) > 0:
                    self.textbox_files_to_convert.configure(state='normal')
                    self.textbox_files_to_convert.delete('1.0', tk.END)
                    for i, row in enumerate(foundFiles):
                        self.textbox_files_to_convert.insert(str(i) + '.0', str(row) + '\n')
                    self.textbox_files_to_convert.configure(state='disabled')
                else:
                    self.textbox_files_to_convert.configure(state='normal')
                    self.textbox_files_to_convert.delete('1.0', tk.END)
                    self.textbox_files_to_convert.insert('1.0',
                                                         'Bemeneti mappa üres, vagy nincsenek benne .txt, illetve .csv fájlok...')
                    self.textbox_files_to_convert.configure(state='disabled')

        # Handle input folder selection
        def open_output_folder():
            self.selected_output_folder = fd.askdirectory()
            self.entry_output_folder.configure(state='normal')
            self.entry_output_folder.insert(0, str(Path(self.selected_output_folder)))
            self.entry_output_folder.configure(state='readonly')

        # Text widget - list of all found files inside the selected INPUT folder
        self.textbox_files_to_convert = st.ScrolledText(self, height=15)
        self.textbox_files_to_convert.insert('1.0',
                                             'Bemeneti mappa üres, vagy nincsenek benne .txt, illetve .csv fájlok...')
        self.textbox_files_to_convert.configure(state='disabled')

        # Browse for input folder
        self.btn_input_browser = ttk.Button(self, text='Válaszd ki a bemeneti mappát', command=open_input_folder)
        self.string_input_folder = tk.StringVar()
        self.entry_input_folder = ttk.Entry(self, textvariable=self.string_input_folder, state='readonly')

        # Browse for output folder
        self.btn_output_browser = ttk.Button(self, text='Válaszd ki a kimeneti mappát', command=open_output_folder)
        self.entry_output_folder = ttk.Entry(self, state='readonly')

        # add padding to the frame and show it
        self.grid(column=0, row=0, padx=10, pady=10, sticky=tk.NW)
        self.textbox_files_to_convert.grid(column=0, row=0, sticky=tk.NW, columnspan=2)
        self.btn_input_browser.grid(column=0, row=1, sticky=tk.NW)
        self.entry_input_folder.grid(column=0, row=2, sticky=tk.NW, ipadx=100)
        self.btn_output_browser.grid(column=0, row=3, sticky=tk.NW, pady=(15, 0))
        self.entry_output_folder.grid(column=0, row=4, sticky=tk.NW, ipadx=100)


class ConversionFrame(ttk.Frame):
    def __init__(self, container, selection_frame):
        super().__init__(container)

        # LabelFrame for Gettext entries
        self.labelFrame_gettext_entries = ttk.LabelFrame(self, text='Gettext adatok')

        # Entry - Translation title
        self.label_translationTitle = ttk.Label(self.labelFrame_gettext_entries, text='Fordítás címe:')
        self.entry_translationTitle = ttk.Entry(self.labelFrame_gettext_entries)

        # Entry - Translator's full name
        self.label_translatorFullName = ttk.Label(self.labelFrame_gettext_entries, text='Fordító teljes neve:')
        self.entry_translatorFullName = ttk.Entry(self.labelFrame_gettext_entries)

        # Entry - Translator's email address
        self.label_translatorEmail = ttk.Label(self.labelFrame_gettext_entries, text='Fordító e-mail címe:')
        self.entry_translatorEmail = ttk.Entry(self.labelFrame_gettext_entries)

        # Entry - Translation target language
        self.label_translationTargetLang = ttk.Label(
            self.labelFrame_gettext_entries,
            text='Fordítási célnyelv angolul:')
        self.string_translationTargetLang = tk.StringVar()
        self.entry_translationTargetLang = ttk.Entry(
            self.labelFrame_gettext_entries,
            textvariable=self.string_translationTargetLang)
        self.entry_translationTargetLang.insert(0, 'Hungarian')

        # Entry - Translation target lang code
        self.label_translationTargetLangCode = ttk.Label(
            self.labelFrame_gettext_entries,
            text='Fordítási célnyelv kódja:')
        self.string_translationTargetLangCode = tk.StringVar()
        self.entry_translationTargetLangCode = ttk.Entry(
            self.labelFrame_gettext_entries,
            textvariable=self.string_translationTargetLangCode)
        self.entry_translationTargetLangCode.insert(0, 'hu')

        # LabelFrame for conversion options
        self.labelFrame_convert_entries = ttk.LabelFrame(self, text='Konvertálás')

        # Label for the option menu
        self.label_conversion_mode = ttk.Label(
            self.labelFrame_convert_entries,
            text='Válaszd ki a konvertálási módot:')

        def conversion_mode_changed(*args):
            container.selected_conversion_mode = self.string_optionMenu_conversion_mode.get()

        # OptionMenu for selecting the conversion mode
        self.conversion_modes = ('.txt -> .pot', '.csv -> .txt')
        self.string_optionMenu_conversion_mode = tk.StringVar()
        self.optionMenu_conversion_mode = ttk.OptionMenu(
            self.labelFrame_convert_entries,
            self.string_optionMenu_conversion_mode,
            self.conversion_modes[0],
            *self.conversion_modes,
            command=conversion_mode_changed
        )

        def conversion_start():
            self.button_startConverting.configure(state='DISABLED')
            if selection.entry_output_folder.get() == '':
                messages.show_warning_msg(
                    'Figyeljé\' oda!',
                    'Nem választottál kimeneti mappát!',
                )
            else:
                if container.selected_conversion_mode == '.txt -> .pot':
                    # Create dictionary from entry data
                    gettext_data = {
                        'input_folder_path': selection.string_input_folder.get(),
                        'output_folder_path': selection.entry_output_folder.get(),
                        'translation_title': self.entry_translationTitle.get(),
                        'translator_full_name': self.entry_translatorFullName.get(),
                        'translator_email': self.entry_translatorEmail.get(),
                        'translation_target_lang': self.string_translationTargetLang.get(),
                        'translation_target_lang_code': self.entry_translationTargetLangCode.get(),
                        'package_version': '1.0'
                    }
                    conversion_result = txtToPot.init_conversion(
                        gettext_data
                    )
                    self.button_startConverting.configure(state='normal')
                elif container.selected_conversion_mode == '.csv -> .txt':
                    # Create dictionary from entry data
                    data = {
                        'input_folder_path': selection.string_input_folder.get(),
                        'output_folder_path': selection.entry_output_folder.get()
                    }
                    conversion_result = csvToTxt.init_conversion(data)

        # Start conversion button
        self.string_conversion_btn = tk.StringVar()
        self.string_conversion_btn.set('Indítás')
        self.button_startConverting = ttk.Button(
            self.labelFrame_convert_entries,
            textvariable=self.string_conversion_btn,
            command=conversion_start)

        # add padding to the frame and show it
        self.grid(column=1, row=0, padx=10, pady=10, sticky=tk.NW)
        self.labelFrame_gettext_entries.grid(column=0, row=0, sticky=tk.W)
        self.label_translationTitle.grid(column=0, row=0, sticky=tk.W)
        self.entry_translationTitle.grid(column=0, row=1, sticky=tk.W)
        self.label_translatorFullName.grid(column=0, row=2, sticky=tk.W)
        self.entry_translatorFullName.grid(column=0, row=3, sticky=tk.W)
        self.label_translatorEmail.grid(column=0, row=4, sticky=tk.W)
        self.entry_translatorEmail.grid(column=0, row=5, sticky=tk.W)
        self.label_translationTargetLang.grid(column=0, row=6, sticky=tk.W)
        self.entry_translationTargetLang.grid(column=0, row=7, sticky=tk.W)
        self.label_translationTargetLangCode.grid(column=0, row=8, sticky=tk.W)
        self.entry_translationTargetLangCode.grid(column=0, row=9, sticky=tk.W)
        self.labelFrame_convert_entries.grid(column=0, row=10, sticky=tk.W, pady=(15, 0))
        self.label_conversion_mode.grid(column=0, row=11, sticky=tk.W)
        self.optionMenu_conversion_mode.grid(column=0, row=12, sticky=tk.W)
        self.button_startConverting.grid(column=0, row=13, sticky=tk.W, pady=(15, 0))


class App(tk.Tk, SelectionFrame):
    def __init__(self):
        super().__init__()

        self.title('Weblate kisegítő')

        # disable menu tearing
        self.option_add('*tearOff', False)

        # disable resize
        self.resizable(False, False)

        # menu bar
        menu_bar = tk.Menu(self)
        self['menu'] = menu_bar
        menu_file = tk.Menu(menu_bar)
        menu_bar.add_cascade(menu=menu_file, label='Fájl')
        menu_file.add_command(label='Kilépés', command=self.destroy)

        # window size
        window_width = 900
        window_height = 440

        # get the screen dimension
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        # find the center point
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)

        # set the position of the window to the center of the screen
        self.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')

        # root column and layout config
        self.columnconfigure(0, weight=2)
        self.columnconfigure(1, weight=2)

        # Set global variables
        self.selected_conversion_mode = '.txt -> .pot'


if __name__ == "__main__":
    app = App()
    selection = SelectionFrame(app)
    conversion = ConversionFrame(app, selection)
    app.mainloop()
