from tkinter.messagebox import showerror, showwarning, showinfo


def show_info_msg(title, message):
    showinfo(
        title=title,
        message=message
    )


def show_warning_msg(title, message):
    showwarning(
        title=title,
        message=message
    )


def show_error_msg(title, message):
    showerror(
        title=title,
        message=message
    )