from pathlib import Path
import messages
import logging
import codecs
import csv
import os


def init_conversion(data):
    # Iterate through all the found .txt files and do the conversion
    if data['input_folder_path'] != '':
        foundFiles = list(Path(data['input_folder_path']).rglob("*.[cC][sS][vV]"))
        if len(foundFiles) > 0:
            for i, row in enumerate(foundFiles):
                result = conversion(row, data)

                if result != 'OK':
                    messages.show_error_msg(
                        'Hiba!',
                        'Hiba történt a fájl konvertálása közben. Nézd meg az error_log.txt fájlt!'
                    )
                    break

            # Show success message if there were no errors
            messages.show_info_msg(
                'Siker!',
                'Kész csoda, de úgy tűnik, sikeres volt a konvertálás!'
            )
        else:
            messages.show_warning_msg(
                'Figyeljé\' oda!',
                'A megadott útvonalon nem található .csv fájl!',
            )
    else:
        messages.show_warning_msg(
            'Figyeljé\' oda!',
            'Nem választottál bemeneti mappát!',
        )


def conversion(csv_file_path, data):
    # Set the correct paths and filenames
    relative_csv_file_path = str(Path(csv_file_path)).replace(str(Path(data['input_folder_path'])), '')
    csv_file_path_no_ext = relative_csv_file_path.replace('.csv', '')
    new_txt_file_path = str(Path(data['output_folder_path'])) + csv_file_path_no_ext

    sentences = {}
    duplicates = []
    previous_location_name = ''

    try:
        with codecs.open(str(Path(csv_file_path)), 'r', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)

            for i, row in enumerate(reader):
                if previous_location_name == row['location']:
                    if row['target'] != '':
                        duplicates.append(row['target'])
                    else:
                        duplicates.append(row['source'])
                else:
                    if len(duplicates) != 0:
                        new_it_num = (i - len(duplicates) - 1)
                        multiple_sentences = sentences.get(new_it_num)
                        for duplicateSentence in duplicates:
                            multiple_sentences += ' ' + duplicateSentence

                        sentences[new_it_num] = multiple_sentences
                        if row['target'] != '':
                            sentences[i] = row['target']
                        else:
                            sentences[i] = row['source']
                        duplicates = []
                    else:
                        if row['target'] != '':
                            sentences[i] = row['target']
                        else:
                            sentences[i] = row['source']
                previous_location_name = row['location']

        csvfile.close()

        os.makedirs(os.path.dirname(str(Path(new_txt_file_path)) + '.txt'), exist_ok=True)
        with codecs.open(str(Path(new_txt_file_path)) + '.txt', 'w', encoding='utf-8') as newTxtFile:
            for i, line in enumerate(sentences.values()):
                newTxtFile.write(line)
                newTxtFile.write('\n')

        newTxtFile.close()
        return 'OK'
    except Exception as err:
        logging.basicConfig(filename='error_log.txt',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            encoding='utf8',
                            level=logging.ERROR)
        logging.exception(".csv to .txt konvertálási hiba.")
        return 'ERROR'
